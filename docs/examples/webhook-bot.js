// npm install -g localtunnel && lt --port 3000
const Telegraf = require('../../telegraf')

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.command('image', (ctx) => ctx.replyWithPhoto({ url: 'https://picsum.photos/200/300/?random' }))
bot.on('text', ({ replyWithHTML }) => replyWithHTML('<b>Hello</b>'))

// Start webhook directly | drop all pending updates { drop_pending_updates: true }
// bot.startWebhook('/secret-path', null, 3000)
// bot.telegram.setWebhook('https://curly-frog-50.loca.lt/secret-path', { drop_pending_updates: true })

// Start webhook via launch method (preffered)
bot.launch({
  webhook: {
    domain: 'https://funny-chicken-45.loca.lt',
    port: 3005
  }
})
